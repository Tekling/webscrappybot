import os
import sys
fpath = os.path.join(os.path.dirname(__file__), 'scripts')
sys.path.append(fpath)
import requests
from bs4 import BeautifulSoup
from company import Company
from webscraper import Webscraper

webby = Webscraper


class Test(object):
    def test_get_indeed(self):
        webby.print_page("software developer", "california")

    def test_get_links(self):
        all_jobs = []
        url = "https://www.indeed.com/jobs?q=software%20developer&l=New%20York%2C%20NY&vjk=ac4785baddd3df46"
        indeed_start = "http://www.indeed.com/"
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        results_link = soup.select(".mosaic-zone a", href=True)
        # for a in results_link:
        #     if "/pagead/clk?mo" in a.get("href", "") and "/cmp/" not in a.get("href", ""):
        #         print(indeed_start + a["href"])
        #         # cmp > reviews of company
        #         # q >selects panel ....addlLoc > redirect panel

    def test_get_company_and_links(self):
        all_jobs = []
        all_links =[]
        url = "https://www.indeed.com/jobs?q=software%20developer&l=New%20York%2C%20NY&vjk=ac4785baddd3df46"
        indeed_start = "http://www.indeed.com/"
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        results_jobs_all = soup.select(".serpContainerMinHeight .mosaic-zone")
        results_jobs = soup.select(".serpContainerMinHeight .mosaic-zone .jobTitle")
        results_title = soup.select(".jobTitle")
        results_company = soup.select(".companyName")
        results_location = soup.select(".companyLocation")
        results_description = soup.select(".job-snippet")
        results_link = soup.select(".mosaic-zone a", href=True)

        for x in range(len(results_jobs)):
            company_x = Company(results_title[x].text if results_title[x] else "N/A",
                                        results_company[x].text if results_company[x] else "N/A",
                                        results_location[x].text if results_location[x] else "N/A",
                                        results_description[x].text if results_description[x] else "N/A")
            all_jobs.append(company_x)
            print(company_x)
            # for a in results_link:
            #     if "/pagead/clk?mo" in a.get("href", "") and "/cmp/" not in a.get("href", ""):
            #         print(indeed_start + a["href"])
            #....if company title->has link add... else ""


        result_count = len(all_jobs)
        print(str(result_count) + " results")
