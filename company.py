class Company:
    def __init__(self, job_title, company_name, location, description):
        self.job_title = job_title
        self.company_name = company_name
        self.location = location
        self.description = description

    def get_name(self):
        return self.job_title

    def set_name(self, job_title):
        self.job_title = job_title

    def get_location(self):
        return self.location

    def set_location(self, location):
        self.location = location

    def get_description(self):
        return self.description

    def set_description(self, description):
        self.description = description

    def __repr__(self):
        return "Job: "+self.job_title+" " \
               "Company Name: " + self.company_name + " " \
               "Location: "+self.location + " " \
               "Description: " + self.description + " "
