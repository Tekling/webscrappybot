import requests
from bs4 import BeautifulSoup
import validators

from company import Company


class Webscraper:
    get_query_job = ""
    get_query_location = ""

    def __init__(self):
        return None

    def print_page(self, get_query_job, get_query_location):
        get_query_job = self.get_query_job
        get_query_location = self.get_query_location
        all_jobs = []
        url_default = "https://www.indeed.com/jobs?q=software%20developer&l=New%20York%2C%20NY&vjk=ac4785baddd3df46"
        url = "https://www.indeed.com/jobs?q=" + get_query_job + "&l=" + get_query_location
        if not validators.url(url):
            url = url_default

        url = url_default
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        results_jobs = soup.select(".serpContainerMinHeight .mosaic-zone .jobTitle")
        results_title = soup.select(".jobTitle")
        results_company = soup.select(".companyName")
        # results_rating = soup.select(".ratingLink")
        # results_salary = soup.findAll(class_="metadata salary-snippet-container")
        results_location = soup.select(".companyLocation")
        results_description = soup.select(".job-snippet")

        for x in range(len(results_jobs)):
            company_x = Company(results_title[x].text if results_title[x] else "N/A",
                                        results_company[x].text if results_company[x] else "N/A",
                                        results_location[x].text if results_location[x] else "N/A",
                                        results_description[x].text if results_description[x] else "N/A")
            all_jobs.append(company_x)
            print(company_x)

        result_count = len(all_jobs)
        print(str(result_count) + " results")

        return all_jobs
