# bot.py
import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
import webscraper

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='!')

load_dotenv()
TOKEN = os.getenv('DISCORD_BOT_TOKEN')


@bot.event
async def on_ready():
    print(f'{bot.user} has connected to Discord!')


@bot.command()
async def status(ctx):
    await ctx.send("bot is online")


@bot.command()
async def indeed(ctx, job, location):
    webby = webscraper.Webscraper()
    job_list = webby.print_page(job, location)

    for single_job in job_list:
        embed1 = discord.Embed(title=single_job.job_title)
        embed1.add_field(name=single_job.company_name, value=single_job.location, inline=True)
        embed1.set_footer(text=single_job.description)
        await ctx.channel.send(embed=embed1)
    result_count = len(job_list)
    await ctx.channel.send(str(result_count)+" results")


bot.run(TOKEN)
